#Settlers of Notre Dame
=======================

Updated: 04/24/16

Group Members

  - Erin Bradford
  - Laura Syers
  - Julianna Yee

---------------------------------------------

##Special libraries needed to build/compile

  - SDL2
  - SDL2\_image

-------------------------------------------

##Instructions on how to build/compile the program

  - Save all of the files in the _final_ folder, including the images folder inside of it
  - Compile the program using the makefile and launch!
      
      $ make
      
      $ ./settlers

-------------------------------------------

##USER MANUAL

After launching the exectuable, the user is greeted with the title screen for
_Settlers of Notre Dame_. To begin playing, the user presses the enter/return key.

The user will use both the terminal and the SDL graphics window to play the game.

At the start, the user can either press "Play Game", "Rules", or "Quit". If they click 
"Rules", an image of the rules of the game will pop up and the user can peruse that for
as long as they want. Once they are ready, they press the enter/return key to go to the 
main menu. Once the user presses "Play Game", they will need to enter how many players are 
playing (2-4) in the terminal. Finally, to begin gameplay, each player takes turns placing 
down 1 dorm and 1 sidewalk on the board. The terminal will let the players know whose 
turn it is. Once each player has placed 2 dorms and 2 adjacent sidewalks, the game begins.

The terminal presents a list of options for the player to choose from. The first thing that
happens is rolling the dice, which happens automatically at the start of each players turn.
The options given are:

  1) View building costs

    * This option tells the user what resources they need to build a sidewalk, a dorm, and a quad

  2) View current resource cards
    
    * This options tells the user how many of each resource they currently possess

  3) Build sidewalk
    
    * This option first checks whether the player has enough resources to build; if they do, it prompts the user to build a sidewalk wherever they choose; otherwise, it tells the player that they need more resources

  4) Build dorm
    
    * This option works the same as the sidewalk option

  5) Build quad
    
    * This option works the same as the sidewalk and dorm options

  6) Trade 3 resources for 1 resource
    
    * This option allows the user to trade in a resource that they have 3+ of for any resource of their choosing; the function checks to make sure that they have enough to trade, first

  7) Check victory point standings
    
    * This option tells the player how many victory points each player currently has

  8) Buy development card

    * This option checks to make sure that the player has enough resource cards to buy the card; the player automatically plays their development card once they purchase it
  
  9) Pass turn
    
    * This option ends the current player's turn and makes it the next player's turn

  10) Quit game
    
    * This option exits the game

The user chooses their option using the terminal and the keyboard. If they do not choose a
valid option, they are prompted to choose one. The game continues on until one of the 
players earns the amount of victory points it takes to win (which is dependent on how many
players are playing).

-------------------------------------------

###User Manual Sparknotes

To start:

  * Enter/return key

At main menu:

  * Click buttons on screen

Select players:

  * Enter number in terminal

First turn(s):

  * Click location on screen

Playing normal turns:

  * Enter choice option in terminal

Building anything:

  * Click location on screen

###Game Pieces

**Sidewalks** : connected to dorms/quads; a person cannot build a new dorm without being next to a 
sidewalk, and a sidewalk cannot be built without being next to a dorm (belonging to the player);
worth no victory points

**Dorms** : connected to sidewalks; for the first turn, the dorm can be placed anywhere, but after
that, the dorm must be adjacent to one of the player's sidewalks; worth one victory point

**Quads** : can only be built on dorms; the player can build a quad if and only if they already 
have a dorm in that spot

**Development Cards** : can be bought at any time; there are 5 types of development cards that
will tell you what to do; they are Year of Plenty, Road Building, Monopoly, Leprechaun, and Victory
Point; if you get any of these cards, the game explains what they do (having 3+ leprechauns and
the most out of all players gives you a victory point)

**Leprechaun** : the Notre Dame-equivalent to the robber; when a 7 is rolled, all players with 7+ 
cards will lose the floor of half of each resource card they own (i.e. if they have 1, they do
not lose anything; if they have 7, they lose 3); in addition, the player is prompted to choose
what number they want to "block" (2-12 but not 7); this means that if that number is rolled, and
any player has a piece on that number, they do not collect resources

**Resource Cards** : used to build sidewalks, dorms, and quads, as well as purchase development 
cards; there are five kinds -- berry (from the DH), bicep (from Rolfs), helmet (from the 
stadium), sun (from the dome), and test (from the library); they are received by placing dorms/
quads onto the pieces that give these resources

-------------------------------------------

#####Known Bugs

If you close the screen before entering the number of players using the x button (of the actual
window), the graphics window closes, but the game in the terminal does not quit; must do ^-C to 
quit the terminal.

If you accidentally put in an input that is not a number, the game basically crashes (runs into
an infinite loop). I.e. we accidentally put 2\ into the program and it broke.
