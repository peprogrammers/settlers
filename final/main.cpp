//Driver program for our Settlers of Catan game
// Julianna Yee, Laura Syers, Erin Bradford

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <stdlib.h>
#include "Graphics.h"
#include <time.h>
#include <unistd.h>
#include "Player.h"
#include "Piece.h"
#include "Board.h"
using namespace std;

int main() {
  //create board
  Board board;
  //play Settlers of Catan
  board.play();

  return 0;
}
