//Erin Bradford, ebradfo2, player.h
//Player header file

#ifndef PLAYER_H
#define PLAYER_H

#include <map>
#include <string>
#include <vector>
#include "Piece.h"

using namespace std;

class Player{

  public:
    Player(); //constructor - initializes everything to zero
    void draw(string); //gets card of a certain type
    void play(string); //plays card of specified type
    void build(Piece); // build dorm/quad on type piece
    int getvpoints(); // returns v points - depends on largest army
    int gettotcards() { return totcards; } //get number of cards player has
    void rob(); // robs half of each type of card player has if they have >= 7 cards
    void addpoints(); // get victory point
    bool checkWin(int); // have you won yet?
    bool hasPiece(int); // do you have this piece?
    bool checkResources(string); //print rescources
    void removeResources(string); // get rid of resources when played
    void tradeResource(); // trade 3 of a resource for one of another
    void print(); // print out players hand
    void printPieces(); //debug - show pieces we have
    int numPieces; //num of pieces player is on
    int stealCards(string); //lose cards of type to give to someone else when monopoly
    int numLep() { return leps; } //size of army
    void addLep(); // new member to army 
    void setLep(bool); // (un)assign largest army card
    bool ifLep() { return lep; } // status of army card
  private:
    map<string,int> cards; //hold resorce cards
    vector<Piece> pieces; //hold pieces player is on
    int vpoints; //victory points player has
    int totcards; //total cards player has
		int leps; //size of army
    bool lep; //army card?
    //helper functions
    string assignChoice(int); //helps trade function
		

};
#endif
