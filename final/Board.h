#ifndef BOARD_H
#define BOARD_H

#include <iostream>
#include <vector>
#include "Piece.h"
#include "Graphics.h"
#include "Player.h"

using namespace std;

class Board {
  public:
    Board();
    void play(); //play game
    void displaySpots(); //print spots array
    void displaySidewalks(); //print sidewalks array
    void placeDorm(int); //place dorm in any location (doesn't need to be next to sidewalk)
    void placeDorm2(int); //place dorm (only if next to sidewalk)
    void placeSidewalk(int,int); //place sidewalk (only if next to dorm)
    void placeQuad(int); //place quad (only if player has dorm there)
    void rollDice(); //roll dice for gameplay
    void giveCards(int); //dole out resource cards depending on roll
    void buildCost(int); //print building costs to user
  private:
    vector<vector<Piece > > board; //composed 2D board of pieces
    int roll; //dice roll
    int numPlayers; 
    Player players[4];
    int spots[6][5]; //keep track of places where dorms can be
    int sidewalks[11][5]; //keep track of places where sidewalks can be
    int lepPos; //number leprechaun blocks
    bool army; //does someone have largest army
    Graphics display; 
    //helper functions
    void playFirst(); //play first turn
    void playTurn(int); //play turn, passing in which player is playing
    int getOpts(int); //display options and return the choice
    void updatePieces(int, int, int); //update player's pieces
    bool checkSpotD(int, int,int,int); //check if dorm is in valid spot
    bool checkSpotS(int, int ,int); //check if sidewalk is in valid spot
    bool checkSpotQ(int, int, int); //check if quad is in valid spot
    void giveDevelopmentCard(int); //randomly give a development card
};

#endif
