Over the break, I would like to explore SDL and do a bunch of tutorials
to get some sort of handle on using SDL in relevant ways to our project.

#RUBRIC
========

Since we do not know how to use SDL at all, we require some learning.
Prof Emrich suggested we use [LazyFoo](lazyfoo.net/tutorials/SDL/index.php).
Lessons that will probably be helpful:

**Lesson 1**: _Hello SDL_, setting up the SDL library

**Lesson 2**: _Getting an Image on the Screen_, self-explanatory

**Lesson 3**: _Event Driven Programming_, allow user to X out a window

**Lesson 4**: _Key Presses_, learn how to handle keyboard input

**Lesson 6**: _Extention Libraries and Loading Other Image Formats_, load png images using SDL_image library

**Lesson 8**: _Geometry Rendering_, making common shapes

**Lesson 17**: _Mouse Events_, read mouse input

+30 pts for completing these lessons

+10 pts for creating a deliverable relevant to the project such as a game piece
